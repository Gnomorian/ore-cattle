/*
 * Copyright (c) 2014, William <w.cameron@crookedhill.co.nz>
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package nz.co.crookedhill.orecattle.mobs;

import net.minecraft.client.model.ModelCow;
import net.minecraft.client.model.ModelPig;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.world.biome.BiomeGenBase;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class OCMobs {
	
	
	public static void init() {
		//REGISTER MOBS
		EntityRegistry.registerGlobalEntityID(OCMobsCowDiamond.class, "DiamonditeCow", 103, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsCowCarbonite.class, "CarboniteCow", 104, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsCowEmerald.class, "EmeraldCow", 105, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsCowEmpowered.class, "EmpoweredCow", 106, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsCowGoldium.class, "GoldiumCow", 107, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsCowIron.class, "IroniteCow", 108, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsCowLazuli.class, "LazuliCow", 109, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsCowQuartzite.class, "QuartzitePig", 110, 113213, 3523523);
		
		EntityRegistry.registerGlobalEntityID(OCMobsPigDiamond.class, "DiamonditePig", 111, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsPigCarbonite.class, "CarbonitePig", 112, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsPigEmerald.class, "EmeraldPig", 113, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsPigEmpowered.class, "EmpoweredPig", 114, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsPigGoldium.class, "GoldiumPig", 115, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsPigIron.class, "IronitePig", 116, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsPigLazuli.class, "LazuliPig", 117, 113213, 3523523);
		EntityRegistry.registerGlobalEntityID(OCMobsPigQuartzite.class, "QuartzitePig", 118, 113213, 3523523);
		
		//REGISTER LANGUAGES
		LanguageRegistry.instance().addStringLocalization("entity.DiamonditeCow.name", "en_US", "Diamondite Cow");
		LanguageRegistry.instance().addStringLocalization("entity.CarboniteCow.name", "en_US", "Carbonite Cow");
		LanguageRegistry.instance().addStringLocalization("entity.EmeraldCow.name", "en_US", "Emeradilian Cow");
		LanguageRegistry.instance().addStringLocalization("entity.EmpoweredCow.name", "en_US", "Empowered Cow");
		LanguageRegistry.instance().addStringLocalization("entity.GoldiumCow.name", "en_US", "Goldium Cow");
		LanguageRegistry.instance().addStringLocalization("entity.IroniteCow.name", "en_US", "Ironite Cow");
		LanguageRegistry.instance().addStringLocalization("entity.LazuliCow.name", "en_US", "Lazuli Cow");
		LanguageRegistry.instance().addStringLocalization("entity.QuartziteCow.name", "en_US", "Quartzite Cow");
		
		LanguageRegistry.instance().addStringLocalization("entity.DiamonditePig.name", "en_US", "Diamondite Pig");
		LanguageRegistry.instance().addStringLocalization("entity.CarbonitePig.name", "en_US", "Carbonite Pig");
		LanguageRegistry.instance().addStringLocalization("entity.EmeraldPig.name", "en_US", "Emeradilian Pig");
		LanguageRegistry.instance().addStringLocalization("entity.EmpoweredPig.name", "en_US", "Empowered Pig");
		LanguageRegistry.instance().addStringLocalization("entity.GoldiumPig.name", "en_US", "Goldium Pig");
		LanguageRegistry.instance().addStringLocalization("entity.IronitePig.name", "en_US", "Ironite Pig");
		LanguageRegistry.instance().addStringLocalization("entity.LazuliPig.name", "en_US", "Lazuli Pig");
		LanguageRegistry.instance().addStringLocalization("entity.QuartzitePig.name", "en_US", "Quartzite Cow");
		
		//REGISTER SPAWN LOCATIONS
		EntityRegistry.addSpawn(OCMobsCowDiamond.class, 10, 1, 3, EnumCreatureType.ambient, BiomeGenBase.extremeHills);
		EntityRegistry.addSpawn(OCMobsCowDiamond.class, 10, 1, 3, EnumCreatureType.ambient, BiomeGenBase.extremeHillsEdge);
		EntityRegistry.addSpawn(OCMobsCowDiamond.class, 10, 1, 3, EnumCreatureType.monster, BiomeGenBase.extremeHills);
		EntityRegistry.addSpawn(OCMobsCowDiamond.class, 10, 1, 3, EnumCreatureType.monster, BiomeGenBase.extremeHillsEdge);
		EntityRegistry.addSpawn(OCMobsCowDiamond.class, 10, 1, 3, EnumCreatureType.ambient, BiomeGenBase.iceMountains);
		EntityRegistry.addSpawn(OCMobsCowDiamond.class, 10, 1, 3, EnumCreatureType.monster, BiomeGenBase.iceMountains);
		EntityRegistry.addSpawn(OCMobsCowQuartzite.class, 10, 1, 3, EnumCreatureType.ambient, BiomeGenBase.hell);
		EntityRegistry.addSpawn(OCMobsCowQuartzite.class, 10, 1, 3, EnumCreatureType.monster, BiomeGenBase.hell);
		
		EntityRegistry.addSpawn(OCMobsPigDiamond.class, 10, 1, 3, EnumCreatureType.ambient, BiomeGenBase.extremeHills);
		EntityRegistry.addSpawn(OCMobsPigDiamond.class, 10, 1, 3, EnumCreatureType.ambient, BiomeGenBase.extremeHillsEdge);
		EntityRegistry.addSpawn(OCMobsPigDiamond.class, 10, 1, 3, EnumCreatureType.monster, BiomeGenBase.extremeHills);
		EntityRegistry.addSpawn(OCMobsPigDiamond.class, 10, 1, 3, EnumCreatureType.monster, BiomeGenBase.extremeHillsEdge);
		EntityRegistry.addSpawn(OCMobsPigDiamond.class, 10, 1, 3, EnumCreatureType.ambient, BiomeGenBase.iceMountains);
		EntityRegistry.addSpawn(OCMobsPigDiamond.class, 10, 1, 3, EnumCreatureType.monster, BiomeGenBase.iceMountains);
		EntityRegistry.addSpawn(OCMobsPigQuartzite.class, 10, 1, 3, EnumCreatureType.ambient, BiomeGenBase.hell);
		EntityRegistry.addSpawn(OCMobsPigQuartzite.class, 10, 1, 3, EnumCreatureType.monster, BiomeGenBase.hell);
	}

}
