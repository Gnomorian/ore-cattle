package nz.co.crookedhill.orecattle.proxy;

import net.minecraft.client.model.ModelCow;
import net.minecraft.client.model.ModelPig;
import nz.co.crookedhill.orecattle.mobs.OCMobsCowCarbonite;
import nz.co.crookedhill.orecattle.mobs.OCMobsCowDiamond;
import nz.co.crookedhill.orecattle.mobs.OCMobsCowEmerald;
import nz.co.crookedhill.orecattle.mobs.OCMobsCowEmpowered;
import nz.co.crookedhill.orecattle.mobs.OCMobsCowGoldium;
import nz.co.crookedhill.orecattle.mobs.OCMobsCowIron;
import nz.co.crookedhill.orecattle.mobs.OCMobsCowLazuli;
import nz.co.crookedhill.orecattle.mobs.OCMobsCowQuartzite;
import nz.co.crookedhill.orecattle.mobs.OCMobsPigCarbonite;
import nz.co.crookedhill.orecattle.mobs.OCMobsPigDiamond;
import nz.co.crookedhill.orecattle.mobs.OCMobsPigEmerald;
import nz.co.crookedhill.orecattle.mobs.OCMobsPigEmpowered;
import nz.co.crookedhill.orecattle.mobs.OCMobsPigGoldium;
import nz.co.crookedhill.orecattle.mobs.OCMobsPigIron;
import nz.co.crookedhill.orecattle.mobs.OCMobsPigLazuli;
import nz.co.crookedhill.orecattle.mobs.OCMobsPigQuartzite;
import nz.co.crookedhill.orecattle.renderer.OCMobsRenderer;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy{
	@Override
	public void registerRenders() {
		//REGISTER RENDERER
				RenderingRegistry.registerEntityRenderingHandler(OCMobsCowDiamond.class, new OCMobsRenderer(new ModelCow(), 0.4F, "Cow_Diamond"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsCowCarbonite.class, new OCMobsRenderer(new ModelCow(), 0.4F, "Cow_Carbonite"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsCowEmerald.class, new OCMobsRenderer(new ModelCow(), 0.4F, "Cow_Emerald"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsCowEmpowered.class, new OCMobsRenderer(new ModelCow(), 0.4F, "Cow_Empowered"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsCowGoldium.class, new OCMobsRenderer(new ModelCow(), 0.4F, "Cow_Goldium"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsCowIron.class, new OCMobsRenderer(new ModelCow(), 0.4F, "Cow_Iron"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsCowLazuli.class, new OCMobsRenderer(new ModelCow(), 0.4F, "Cow_Lazuli"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsCowQuartzite.class, new OCMobsRenderer(new ModelCow(), 0.4F, "Cow_Quartzite"));
				
				RenderingRegistry.registerEntityRenderingHandler(OCMobsPigDiamond.class, new OCMobsRenderer(new ModelPig(), 0.4F,"Pig_Diamond"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsPigCarbonite.class, new OCMobsRenderer(new ModelPig(), 0.4F, "Pig_Carbonite"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsPigEmerald.class, new OCMobsRenderer(new ModelPig(), 0.4F, "Pig_Emerald"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsPigEmpowered.class, new OCMobsRenderer(new ModelPig(), 0.4F, "Pig_Empowered"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsPigGoldium.class, new OCMobsRenderer(new ModelPig(), 0.4F, "Pig_Goldium"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsPigIron.class, new OCMobsRenderer(new ModelPig(), 0.4F, "Pig_Iron"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsPigLazuli.class, new OCMobsRenderer(new ModelPig(), 0.4F, "Pig_Lazuli"));
				RenderingRegistry.registerEntityRenderingHandler(OCMobsPigQuartzite.class, new OCMobsRenderer(new ModelPig(), 0.4F, "Pig_Quartzite"));
	}
}
